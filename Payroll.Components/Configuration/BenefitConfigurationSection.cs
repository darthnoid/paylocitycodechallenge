﻿using Payroll.Common.Entities.Benefits;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Components.Configuration
{
    public class BenefitConfigurationSection : ConfigurationSection
    {
        private const string _benefitCollectionKey = "benefits";
        private const string _benefitElementKey = "benefitSetting";

        [ConfigurationProperty(_benefitCollectionKey)]
        [ConfigurationCollection(typeof(BenefitSettingElement), AddItemName = _benefitCollectionKey, CollectionType = ConfigurationElementCollectionType.BasicMap)]
        public BenefitSettingsCollection Benefits
        {
            get
            {
                return (BenefitSettingsCollection)base[_benefitCollectionKey];
            }

        }
    }
}
