﻿using Payroll.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Components.Configuration
{
    public class PayrollConfigurationSection : ConfigurationSection
    {
        private const string _payPeriodsPerYearKey = "payPeriodsPerYear";

        private const string _grossPayPerPeriodKey = "grossPayPerPeriod";

        [ConfigurationProperty(_payPeriodsPerYearKey, IsRequired = true)]
        public int PayPeriodsPerYear
        {
            get
            {
                return (int)this[_payPeriodsPerYearKey];
            }
        }

        [ConfigurationProperty(_grossPayPerPeriodKey, IsRequired = true)]
        public decimal GrossPayPerPeriod
        {
            get
            {
                return (decimal)this[_grossPayPerPeriodKey];
            }
        }
    }
}
