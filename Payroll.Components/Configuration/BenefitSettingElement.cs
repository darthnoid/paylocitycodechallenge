﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payroll.Components.Configuration
{
    public class BenefitSettingElement : ConfigurationElement
    {
        private const string _packageNameKey = "packageName";
        private const string _annualEmployeeCostKey = "annualEmployeeCost";
        private const string _annualDependentCostKey = "annualDependentCost";

        [ConfigurationProperty(_packageNameKey, IsRequired = true, IsKey = true)]
        public string PackageName
        {
            get
            {
                return this[_packageNameKey] as string;
            }

            set
            {
                this[_packageNameKey] = value;
            }
        }

        [ConfigurationProperty(_annualEmployeeCostKey, IsRequired = true)]
        public decimal AnnualEmployeeCost
        {
            get
            {
                return (decimal)this[_annualEmployeeCostKey];
            }
            
            set
            {
                this[_annualEmployeeCostKey] = value;
            }
        }

        [ConfigurationProperty(_annualDependentCostKey, IsRequired = true)]
        public decimal AnnualDependentCost
        {
            get
            {
                return (decimal)this[_annualDependentCostKey];
            }

            set
            {
                this[_annualDependentCostKey] = value;
            }
        }
    }
}
