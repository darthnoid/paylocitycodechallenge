﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace Payroll.Components.Configuration
{
    [ConfigurationCollection(typeof(BenefitSettingElement))]
    public class BenefitSettingsCollection : ConfigurationElementCollection, IEnumerable<BenefitSettingElement>
    {
        private const string _elementName = "benefitSetting";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }

        protected override string ElementName
        {
            get
            {
                return _elementName;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new BenefitSettingElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var setting = (BenefitSettingElement)element;
            return setting.PackageName;
        }


        protected override bool IsElementName(string elementName)
        {
            if (string.IsNullOrWhiteSpace(elementName))
                return false;

            return elementName.Equals(this.ElementName, StringComparison.OrdinalIgnoreCase);
        }

        IEnumerator<BenefitSettingElement> IEnumerable<BenefitSettingElement>.GetEnumerator()
        {
            return BenefitSettings.GetEnumerator();
        }

        public IEnumerable<BenefitSettingElement> BenefitSettings
        {
            get
            {
                foreach (BenefitSettingElement element in this)
                {
                    if (element != null)
                        yield return element;
                }
            }
        }
    }
}