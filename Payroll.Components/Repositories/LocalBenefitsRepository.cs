﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Payroll.Common.Entities.Benefits;
using Payroll.Common.Interfaces;
using Payroll.Components.Configuration;

namespace Payroll.Components.BusinessLogic
{
    public class LocalBenefitRepository : IBenefitRepository
    {
        private const string _configurationElementName = "benefitConfiguration";

        /// <summary>
        /// Gets the benefit Packages configured by the local application config file.
        /// </summary>
        /// <returns></returns>
        public List<BenefitPackage> GetBenefitPackages()
        {
            var benefitPackages = new List<BenefitPackage>();
            var benefitSection = ConfigurationManager.GetSection(_configurationElementName) as BenefitConfigurationSection;

            if (benefitSection == null || benefitSection.Benefits == null) return benefitPackages;

            benefitSection.Benefits.BenefitSettings.ToList().ForEach(benefit =>
            {
                benefitPackages.Add(new BenefitPackage(benefit.PackageName, new Cost(benefit.AnnualEmployeeCost), new Cost(benefit.AnnualDependentCost)));
            });

            return benefitPackages;
        }
    }
}
