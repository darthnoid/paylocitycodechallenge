﻿using System;
using System.Configuration;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.Configuration;

namespace Payroll.Components.BusinessLogic
{
    public class LocalPayrollConfigurationRepository : IPayrollConfigurationRepository
    {
        private const string _configurationElementName = "payrollConfiguration";

        /// <summary>
        /// Gets the payroll configuration parameters defined in the local application config file.
        /// </summary>
        /// <returns></returns>
        public IPayrollConfiguration GetConfiguration()
        {
            var section = ConfigurationManager.GetSection(_configurationElementName) as PayrollConfigurationSection;

            if (section == null) throw new InvalidOperationException(String.Format("No {0} section was defined in the config file.", _configurationElementName));

            return new PayrollConfiguration()
            {
                GrossPayPerPeriod = section.GrossPayPerPeriod,
                PayPeriodsPerYear = section.PayPeriodsPerYear
            };
        }
    }
}
