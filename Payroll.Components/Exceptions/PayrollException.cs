﻿using System;
using System.Runtime.Serialization;

namespace Payroll.Components.Exceptions
{
    [Serializable]
    public class PayrollException : Exception
    {
        public PayrollException()
        { }

        public PayrollException(string message)
            : base(message)
        { }

        public PayrollException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public PayrollException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo, context)
        { }
    }
}
