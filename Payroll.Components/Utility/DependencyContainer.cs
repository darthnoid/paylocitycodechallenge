﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Payroll.Components.Utility
{
    /// <summary>
    /// Thread-safely lazily intitialized wrapper around unity.
    /// </summary>
    public static class DependencyContainer
    {
        private static readonly Lazy<IUnityContainer> _container = new Lazy<IUnityContainer>(() => new UnityContainer().LoadConfiguration());

        /// <summary>
        /// Constructs the registered type that implements the interface T.
        /// </summary>
        /// <typeparam name="T">The interface to be resolved.</typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            return _container.Value.Resolve<T>();
        }

        /// <summary>
        /// Registers a concrete type to it's interface for resolution at run time.
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <typeparam name="TConcrete"></typeparam>
        public static void RegisterType<TInterface, TConcrete>()
            where TConcrete : TInterface, new()
        {
            _container.Value.RegisterType<TInterface, TConcrete>();
        }
    }
}
