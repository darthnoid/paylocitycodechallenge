﻿using Payroll.Common.Entities;
using Payroll.Common.Interfaces;
using Payroll.Components.Utility;

namespace Payroll.Components.BusinessLogic
{
    public class EmployerConfigurationLogic 
    {
        private readonly IPayrollConfigurationRepository _payrollConfigurationRepository;
        private readonly IBenefitRepository _benefitRepository;

        public EmployerConfigurationLogic()
        {
            this._payrollConfigurationRepository = DependencyContainer.Resolve<IPayrollConfigurationRepository>();
            this._benefitRepository = DependencyContainer.Resolve<IBenefitRepository>();
        }

        /// <summary>
        /// Gets the configuration for the current employer environment.
        /// </summary>
        /// <returns></returns>
        public EmployerSettings GetSettings()
        {
            return new EmployerSettings(this._payrollConfigurationRepository.GetConfiguration(), this._benefitRepository.GetBenefitPackages());
        }
    }
}
