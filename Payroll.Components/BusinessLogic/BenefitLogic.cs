﻿using System;
using System.Collections.Generic;
using System.Linq;
using Payroll.Common.Entities.Benefits;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.BusinessLogic.PayrollRules;
using Payroll.Components.Utility;

namespace Payroll.Components.BusinessLogic
{
    using DiscountRates = Dictionary<IPerson, decimal>;

    public class BenefitLogic
    {
        private IBenefitRepository _data;

        public BenefitLogic()
        {
            this._data = DependencyContainer.Resolve<IBenefitRepository>();
        }

        public BenefitLogic(IBenefitRepository benefitRepository)
        {
            this._data = benefitRepository;
        }

        /// <summary>
        /// Get an employees benefits and their costs after factoring in eligible discounts.
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public List<BenefitPackage> GetBenefits(Employee employee)
        {
            var packages = this._data.GetBenefitPackages();

            SetPrices(employee, packages);

            return packages;
        }

        private static void SetPrices(Employee employee, List<BenefitPackage> activeBenefits)
        {
            var discountRates = GetDiscountRates(employee);

            activeBenefits.ForEach(package =>
            {
                Cost employeeBaseCost = package.EmployeeCost;
                Cost dependentBaseCost = package.DependentCost;

                package.EmployeeCost = GetAdjustedCost(employeeBaseCost, discountRates[employee]);

                var dependentCosts = employee.Dependents.Select(dependent => GetAdjustedCost(dependentBaseCost, discountRates[dependent]));
                package.DependentCost = Cost.Combine(dependentCosts);
            });
        }

        private static DiscountRates GetDiscountRates(Employee employee)
        {
            var discountRules = DiscountLogic.GetAllDiscountRules();

            DiscountRates discounts = new DiscountRates();
            discounts.Add(employee, GetDiscountRate(employee, discountRules));

            employee.Dependents.ForEach(dep =>
            {
                discounts.Add(dep, GetDiscountRate(dep, discountRules));
            });

            return discounts;
        }

        private static decimal GetDiscountRate(IPerson person, List<IDiscountRule> discountRules)
        {
            decimal discountModifier = 0m;

            discountRules.ForEach(rule => discountModifier += rule.GetDiscountPercentage(person));

            return Math.Min(1.0m, discountModifier);
        }

        private static Cost GetAdjustedCost(Cost baseCost, decimal discountPercentage)
        {
            decimal discountDollars = Decimal.Multiply(baseCost.GrossCost, discountPercentage);
            return new Cost(baseCost.GrossCost, discountDollars);
        }
    }
}
