﻿using System;
using System.Collections.Generic;
using System.Linq;
using Payroll.Common.Entities.Benefits;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.Exceptions;

namespace Payroll.Components.BusinessLogic
{
    internal class PayrollProcessor
    {
        private readonly IPayrollConfiguration _configuration;
        private readonly DeductionProcessor _deductionProcessor;

        public PayrollProcessor(IPayrollConfiguration configuration)
        {
            this._configuration = configuration;
            this._deductionProcessor = new DeductionProcessor(configuration);
        }

        /// <summary>
        /// Creates a paycheck for the given employee.
        /// </summary>
        /// <param name="employee">Employee being paid.</param>
        /// <param name="payPeriod">Annual pay period of the paycheck.</param>
        /// <param name="salary">Gross annual employee salary.</param>
        /// <param name="enrolledBenefits">Benefits in which the employee is enrolled.</param>
        /// <returns></returns>
        public Paycheck CreatePaycheck(Employee employee, int payPeriod, decimal salary, List<BenefitPackage> enrolledBenefits)
        {
            const int roundingPlaces = 2;

            var deductions = GetPayrollDeductions(employee, payPeriod, enrolledBenefits);

            decimal grossPay = Decimal.Round(Decimal.Divide(salary, _configuration.PayPeriodsPerYear), roundingPlaces);

            if (deductions.Sum(d => d.Cost) > grossPay) throw new PayrollException("Deductions exceed the employees gross pay.");

            return new Paycheck(payPeriod, grossPay, deductions);
        }

        private List<PayrollDeduction> GetPayrollDeductions(Employee employee, int payPeriod, List<BenefitPackage> enrolledBenefits)
        {
            return this._deductionProcessor.GetDeductions(employee, payPeriod, enrolledBenefits);
        }
    }
}
