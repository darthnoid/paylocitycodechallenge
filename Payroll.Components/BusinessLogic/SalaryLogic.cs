﻿using System;
using Payroll.Common.Interfaces;
using Payroll.Components.Utility;

namespace Payroll.Components.BusinessLogic
{
    internal class SalaryLogic 
    {
        private readonly IPayrollConfiguration _configuration;

        internal SalaryLogic()
        {
            this._configuration = DependencyContainer.Resolve<IPayrollConfigurationRepository>().GetConfiguration();
        }

        internal SalaryLogic(IPayrollConfiguration configuration)
        {
            this._configuration = configuration;
        }

        /// <summary>
        /// Gets the gross salary based on the payroll configuration.
        /// </summary>
        /// <returns></returns>
        internal decimal GetGrossSalary()
        {
            return Math.Round(Decimal.Multiply(this._configuration.GrossPayPerPeriod, this._configuration.PayPeriodsPerYear), 2);
        }
    }
}
