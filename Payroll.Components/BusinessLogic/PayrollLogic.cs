﻿using System;
using System.Collections.Generic;
using Payroll.Common.Entities.Benefits;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.Utility;

namespace Payroll.Components.BusinessLogic
{
    public class PayrollLogic
    {
        private readonly IPayrollConfigurationRepository _payrollConfigData;

        public PayrollLogic(IPayrollConfigurationRepository payrollConfigData)
        {
            this._payrollConfigData = payrollConfigData;
        }

        public PayrollLogic()
        {
            _payrollConfigData = DependencyContainer.Resolve<IPayrollConfigurationRepository>();
        }

        private static void AssertEmployeeIsValid(Employee employee)
        {
            if (employee == null) throw new ArgumentNullException(nameof(employee));
            if (String.IsNullOrWhiteSpace(employee.FirstName)) throw new ArgumentException("employee.FirstName is null or empty.");
            if (String.IsNullOrWhiteSpace(employee.LastName)) throw new ArgumentException("employee.LastName is null or empty.");

            employee.Dependents.ForEach(dependent =>
            {
                if (String.IsNullOrWhiteSpace(dependent.FirstName)) throw new ArgumentException("dependent.FirstName is null or empty.");
                if (String.IsNullOrWhiteSpace(dependent.LastName)) throw new ArgumentException("dependent.LastName is null or empty.");
            });
        }

        /// <summary>
        /// Create a preview of the employees annual payschedule.
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public PayrollInformation CreatePreview(Employee employee)
        {
            AssertEmployeeIsValid(employee);

            var configuration = this._payrollConfigData.GetConfiguration();

            decimal salary = new SalaryLogic(configuration).GetGrossSalary();

            var enrolledBenefits = new BenefitLogic().GetBenefits(employee);

            var paychecks = CreatePaySchedule(configuration, employee, salary, enrolledBenefits);

            return new PayrollInformation(employee, paychecks, enrolledBenefits);
        }

        private static List<Paycheck> CreatePaySchedule(IPayrollConfiguration configuration, Employee employee, decimal salary, List<BenefitPackage> enrolledBenefits)
        {
            var paychecks = new List<Paycheck>();
            var payrollProcessor = new PayrollProcessor(configuration);

            for (int i = 0; i < configuration.PayPeriodsPerYear; i++)
            {
                var check = payrollProcessor.CreatePaycheck(employee, i + 1, salary, enrolledBenefits);
                paychecks.Add(check);
            }

            return paychecks;
        }
    }
}
