﻿using System;
using Payroll.Common.Interfaces;

namespace Payroll.Components.BusinessLogic.PayrollRules
{
    public class NameBeginsWithADiscount : DiscountRule
    {
        private const decimal _discountPercentage = 0.1m;

        /// <summary>
        /// Returns the percentage (between 0.0 and 1.0) that the person is eligible for.
        /// </summary>
        /// <param name="person">The person being being evaluated.</param>
        /// <returns></returns>
        public override decimal GetDiscountPercentage(IPerson person)
        {
            if (person == default(IPerson)) throw new ArgumentNullException(nameof(person));

            if (MeetsDiscountCriteria(person))
                return _discountPercentage;

            return 0m;
        }

        private static bool MeetsDiscountCriteria(IPerson person)
        {
            return person.FirstName.StartsWith("A", StringComparison.InvariantCultureIgnoreCase) || person.LastName.StartsWith("A", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
