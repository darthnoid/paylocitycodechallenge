﻿using Payroll.Common.Interfaces;

namespace Payroll.Components.BusinessLogic.PayrollRules
{
    /// <summary>
    /// Used to evaluate if a what the discount for a given benefit package is for a person.
    /// </summary>
    public abstract class DiscountRule : IDiscountRule
    {
        abstract public decimal GetDiscountPercentage(IPerson person);
    }
}
