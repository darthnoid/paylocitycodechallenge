﻿using Payroll.Common.Interfaces;

namespace Payroll.Components.BusinessLogic.PayrollRules
{
    public interface IDiscountRule
    {
        /// <summary>
        /// Returns the percentage (between 0.0 and 1.0) that the person is eligible for.
        /// </summary>
        /// <param name="person">The person being being evaluated.</param>
        /// <returns></returns>
        decimal GetDiscountPercentage(IPerson person);
    }
}
