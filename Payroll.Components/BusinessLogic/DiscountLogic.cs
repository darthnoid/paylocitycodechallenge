﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Payroll.Components.BusinessLogic.PayrollRules;

namespace Payroll.Components.BusinessLogic
{
    public static class DiscountLogic
    {
        /// <summary>
        /// Gets all discount rules (classes implementing IDiscountRule) defined in this assembly.
        /// </summary>
        /// <returns></returns>
        public static List<IDiscountRule> GetAllDiscountRules()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var types = (from type in assembly.GetTypes()
                         where !type.IsAbstract && type.GetInterfaces().Contains(typeof(IDiscountRule))
                         && type.GetConstructor(Type.EmptyTypes) != null
                         select type).ToList();

            return types.Select(type => (IDiscountRule)Activator.CreateInstance(type)).ToList();
        }
    }
}
