﻿using System;
using System.Collections.Generic;
using Payroll.Common.Entities.Benefits;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.Utility;

namespace Payroll.Components.BusinessLogic
{
    internal class DeductionProcessor
    {
        private readonly IPayrollConfiguration _payrollConfiguration;

        internal DeductionProcessor(IPayrollConfiguration payrollConfiguration)
        {
            this._payrollConfiguration = payrollConfiguration;
        }

        internal DeductionProcessor()
        {
            this._payrollConfiguration = DependencyContainer.Resolve<IPayrollConfigurationRepository>().GetConfiguration();
        }

        /// <summary>
        /// Gets the payroll deductions for a given employee, during a given pay period, given a set of benefits.
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="payPeriod">The pay period the charges are being deducted for.</param>
        /// <param name="benefits">The benefits being deducted and their associated annual costs.</param>
        /// <returns></returns>
        internal List<PayrollDeduction> GetDeductions(Employee employee, int payPeriod, List<BenefitPackage> benefits)
        {
            if (employee == null) throw new ArgumentNullException(nameof(employee));
            if (benefits == null) throw new ArgumentNullException(nameof(benefits));

            var deductions = new List<PayrollDeduction>();

            foreach (var benefit in benefits)
            {
                deductions.Add(GetDeduction(benefit.EmployeeCost.NetCost, payPeriod, this._payrollConfiguration.PayPeriodsPerYear));

                if (employee.Dependents.Count > 0)
                    deductions.Add(GetDeduction(benefit.DependentCost.NetCost, payPeriod, this._payrollConfiguration.PayPeriodsPerYear));
            }

            return deductions;
        }

        private static decimal GetBenefitCostByPayPeriod(decimal benefitCost, int currentPayPeriod, int annualPayPeriodCount)
        {
            const int roundingPlaces = 2;

            bool finalPayPeriod = annualPayPeriodCount == currentPayPeriod;

            decimal baseCostPerPayPeriod = Math.Round(Decimal.Divide(benefitCost, annualPayPeriodCount), roundingPlaces);
            decimal roundingError = 0m;

            if (finalPayPeriod)
                roundingError = benefitCost - Decimal.Multiply(baseCostPerPayPeriod, annualPayPeriodCount);

            return baseCostPerPayPeriod + roundingError;
        }

        private static PayrollDeduction GetDeduction(decimal netCost, int payPeriod, int annualPayPeriodCount)
        {
            return new PayrollDeduction(GetBenefitCostByPayPeriod(netCost, payPeriod, annualPayPeriodCount));
        }
    }
}
