﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using Payroll.Common.Entities.Payroll;
using Payroll.Components.BusinessLogic;

namespace PayrollWebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class PayrollController : ApiController
    {
        [Route("api/payroll/getEmployerSettings")]
        public IHttpActionResult GetEmployerSettings()
        {
            try
            {
                var employerConfigLogic = new EmployerConfigurationLogic();
                var settings = employerConfigLogic.GetSettings();

                return Ok(settings);
            }
            catch (Exception)
            {
                // log real error to logging api, return something with fewer web server details.
                return InternalServerError(new InvalidOperationException("Failed to retrieve employer configuration settings."));
            }
        }

        [Route("api/payroll/createPayrollPreview")]
        public IHttpActionResult CreatePayrollPreview(Employee employee)
        {
            try
            {
                var payrollLogic = new PayrollLogic();

                var preview = payrollLogic.CreatePreview(employee);

                return Ok(preview);
            }
            catch (ArgumentException)
            {
                return BadRequest("Unable to create payroll preview for the employee.");
            }
            catch (Exception)
            {
                // log real error to logging api, return something with fewer web server details.
                return InternalServerError();
            }
        }
    }
}
