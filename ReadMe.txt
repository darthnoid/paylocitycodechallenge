Matt Jindra
matthewjindra@gmail.com
567-698-1547
-----------

Published to AWS at:
http://payrollcalculator-test.us-west-2.elasticbeanstalk.com/

To run locally:
1.) Web Api and Web UI are both set up as start up projects. Open the .sln file and hit f5/ctrl f5 to start.

Visual Studio 2015
AngularJS 1.6, Angular Material
ASP.NET WebApi
.Net 4.6.1

Notes:
- If loading one or more of the projects fails when opening the solution it's probably because your computer doesn't
  want to load an untrusted project. Usually there's a dialog, if for some reason there isn't. Right click on the project
  and press 'Reload project'.