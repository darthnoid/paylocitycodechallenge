﻿(function () {
    "use strict";

    // At some point we may want to get employees from an API call. This is to wrap the creation and retreival of employees.
    angular.module('payrollCalculatorApp').service('employeeService', function () {

        var employees = [];

        this.getEmployees = function () {
            return employees;
        }

        this.addEmployee = function (employee) {
            employee.id = employees.length;
            employees.push(employee);
        }

        this.deleteEmployee = function (employee) {
            let idx = employees.indexOf(employee);

            if (idx > -1) {
                employees.splice(idx, 1);
            }
        }

        this.updateEmployee = function (existingEmployee, updatedEmployee) {
            employees[existingEmployee.id] = updatedEmployee;
        }
    });
})();
