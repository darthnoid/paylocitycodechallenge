﻿(function () {
    "use strict";

    angular.module('payrollCalculatorApp').service('payrollService', function ($http, $mdToast) {
        var endpoint = apiEndpoint + 'payroll/';

        function errorHandler(response) {
            $mdToast.show($mdToast.simple().textContent('An error occurred, sorry! Try again later!'));
            return response.data;
        }

        function responseHandler(response) {
            return response.data;
        }

        this.getPayrollSettings = function () {
            return $http.get(endpoint + 'getEmployerSettings').then(responseHandler, errorHandler);
        }

        this.getPayrollPreview = function (employee) {
            return $http.post(endpoint + 'createPayrollPreview', employee).then(responseHandler, errorHandler);
        }
    });
})();
