﻿(function () {
    "use strict";

    angular.module('payrollCalculatorApp', ['utils.autofocus', 'ui.grid', 'ui.grid.autoResize', 'ui.grid.selection', 'ngMaterial']);
})();