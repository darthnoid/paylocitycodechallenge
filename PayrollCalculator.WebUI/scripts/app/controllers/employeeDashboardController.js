﻿(function () {
    "use strict";
    angular.module('payrollCalculatorApp').controller('employeeDashboardController', ['$scope', '$window', 'employeeService', 'payrollService', '$mdDialog',
        function ($scope, $window, employeeService, payrollService, $mdDialog) {

        // table set up
        $scope.employeeGridOptions = {
            enableColumnMenus: false,
            enableRowSelection: true,
            enableSelectAll: false,
            enableFullRowSelection: true,
            multiSelect: false,
            columnDefs: [
                { name: 'FirstName', enableHiding: false },
                { name: 'LastName', enableHiding: false },
                { name: 'Dependents.length', displayName: "Number of Dependents", enableHiding: false }
            ]
        };

        $scope.payrollGridOptions = {
            enableColumnMenus: false,
            columnDefs: [
                { name: 'PayPeriod' },
                { name: 'GrossPay', cellFilter: 'currency'},
                { field: 'Deductions', displayName: 'Payroll Deductions', cellTemplate: '<span>{{row.entity.GrossPay-row.entity.NetPay | currency}}</span>'},
                { name: 'NetPay', cellFilter: 'currency'}
            ],
            data: []
        };

        var employeeModalOptions = {
            scope: $scope,
            preserveScope: true,
            controllerAs: 'employeeModalController',
            templateUrl: 'employeeDialog.tmpl.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false
        };

        var showEmployeeWindow = function (employee) {
            $scope.employeeToModify = employee;
            $mdDialog.show(employeeModalOptions).then(function () {
                refreshEmployees();
            });
        }

        $scope.openNewEmployeeWindow = function () {
            showEmployeeWindow(null);
        }

        //refresh table when employeeDialog closes.
        var refreshEmployees = function () {
            $scope.employeeGridOptions.data = employeeService.getEmployees();
            $scope.preview = null;
        }

        // register selection event.
        $scope.employeeGridOptions.onRegisterApi = function (gridApi) {
            $scope.employeeGridOptions = gridApi;

            $scope.selectedRow = function () {
                let rows = $scope.employeeGridOptions.selection.getSelectedRows();
                if (rows.length > 0)
                    return rows[0];
            }

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.getPreview();
            });
        }

        $scope.openSelectedEmployee = function () {
            showEmployeeWindow($scope.selectedRow());
        }

        $scope.removeSelectedEmployee = function () {
            employeeService.deleteEmployee($scope.selectedRow());
            refreshEmployees();
        }

        $scope.getPreview = function () {
            $scope.loading = true;
            $scope.preview = null;

            let selectedRow = $scope.selectedRow();
            if (selectedRow != undefined) {
                payrollService.getPayrollPreview(selectedRow)
                    .then(function (data) {
                        $scope.preview = data;
                        console.log(data);
                        $scope.payrollGridOptions.data = data.Paychecks;
                        $scope.loading = false;
                    });
            } else {
                $scope.loading = false;
            }
        }

        refreshEmployees();
    }]);
})();

