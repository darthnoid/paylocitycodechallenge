﻿(function () {
    "use strict";

    angular.module('payrollCalculatorApp').controller('settingsController', ['$scope', '$window', '$mdDialog', 'payrollService', function ($scope, $window, $mdDialog, payrollService) {

        $scope.showSettings = function () {
            openSettingsModal();
        }
         
        function openSettingsModal() {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'settingsDialog.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            });
        };

        function DialogController($scope, $mdDialog) {
            $scope.loading = true;
            $scope.benefitGridSettings = {
                enableColumnMenus: false,
                columnDefs: [
                    { name: 'Name' },
                    { name: 'EmployeeCost.GrossCost', displayName: 'Employee Cost', cellFilter: 'currency' },
                    { name: 'DependentCost.GrossCost', displayName: 'Dependent Cost', cellFilter: 'currency' }
                ]
            }

            $scope.hide = function () {
                $mdDialog.hide();
            };

            function getPayrollSettings() {
                $scope.payrollSettings = {};

                payrollService.getPayrollSettings().then(function (data) {
                    $scope.payrollSettings = data;
                    $scope.benefitGridSettings.data = data.BenefitPackages;
                    $scope.loading = false;
                });
            }

            getPayrollSettings();
        }
    }]);

})();

