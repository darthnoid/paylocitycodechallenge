﻿(function () {
    "use strict";

    angular.module('payrollCalculatorApp').controller('employeeModalController', ['$scope', '$window', '$mdDialog', 'employeeService', function ($scope, $window, $mdDialog, employeeService) {

        // employeeToModify should be set in the parent scope for this modal.
        var existingCopyOfEmployee = angular.copy($scope.employeeToModify);

        $scope.employee = angular.copy(existingCopyOfEmployee);

        $scope.dialogMode = "Create";

        if (existingCopyOfEmployee) {
            $scope.dialogMode = "Update";
        }

        if ($scope.employee == undefined) $scope.employee = {
            Dependents: []
        };

        $scope.cancel = function () {
            // using hide as $mdDialog.cancel() currently throws an exception due to an angular bug
            $mdDialog.hide();
        }

        $scope.addEmployee = function () {
            employeeService.addEmployee($scope.employee);
            $mdDialog.hide();
        }

        $scope.updateEmployee = function () {
            employeeService.updateEmployee(existingCopyOfEmployee, $scope.employee);
            $mdDialog.hide();
        }

        $scope.makeNewDependent = function () {
            if ($scope.employee.Dependents == undefined) {
                $scope.employee.Dependents = [];
            }

            $scope.employee.Dependents.push({});
        }

        $scope.removeDependent = function (dependent) {
            let dependents = $scope.employee.Dependents;
            let idx = dependents.indexOf(dependent);

            if (idx > -1) {
                dependents.splice(idx, 1);
                $scope.employeeForm.$setDirty();
            }
        }
    }]);
})();
