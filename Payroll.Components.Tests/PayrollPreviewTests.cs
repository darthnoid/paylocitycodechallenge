﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Common.Entities;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.BusinessLogic;
using Payroll.Components.Utility;

namespace Payroll.Components.Tests
{
    [TestClass]
    public class PayrollPreviewTests
    {
        [TestMethod]
        public void PayrollPreview_NoDiscountOnSoleEmployee()
        {
            DependencyContainer.RegisterType<IBenefitRepository, LocalBenefitRepository>();
            var benefits = DependencyContainer.Resolve<IBenefitRepository>().GetBenefitPackages();

            var logic = new PayrollLogic();
            var employee = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            var result = logic.CreatePreview(employee);
            Assert.IsNotNull(result, "Payroll preview result is not null.");
            Assert.IsNotNull(result.Employee, "Employee on payroll preview is not null");
            Assert.IsNotNull(result.Paychecks, "Paychecks on payroll preview is not null");
            Assert.IsTrue(result.Paychecks.Count > 0, "Paychecks on payroll preview has records.");

            Assert.AreEqual(benefits.Sum(b => b.EmployeeCost.NetCost), result.PayrollDeductions);
        }

        [TestMethod]
        public void PayrollPreview_DeductionsAddUpToTotal_Single()
        {
            DependencyContainer.RegisterType<IBenefitRepository, LocalBenefitRepository>();
            var benefits = DependencyContainer.Resolve<IBenefitRepository>().GetBenefitPackages();

            var employee = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            var result = new PayrollLogic().CreatePreview(employee);
            decimal totalDeductionsOnEntity = result.PayrollDeductions;
            decimal totalDeductionsOnPaychecks = result.Paychecks.Sum(p => p.PayrollDeductions.Sum(d => d.Cost));

            Assert.AreEqual(totalDeductionsOnEntity, totalDeductionsOnPaychecks);
        }

        [TestMethod]
        public void PayrollPreview_DeductionsAddUpToTotal_Dependent()
        {
            DependencyContainer.RegisterType<IBenefitRepository, LocalBenefitRepository>();
            var benefits = DependencyContainer.Resolve<IBenefitRepository>().GetBenefitPackages();

            var employee = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            employee.Dependents.Add(new Dependent()
            {
                FirstName = "Luke",
                LastName = "Skywalker"
            });

            var result = new PayrollLogic().CreatePreview(employee);
            decimal totalDeductionsOnEntity = result.PayrollDeductions;
            decimal totalDeductionsOnPaychecks = result.Paychecks.Sum(p => p.PayrollDeductions.Sum(d => d.Cost));

            Assert.AreEqual(totalDeductionsOnEntity, totalDeductionsOnPaychecks);
        }

        [TestMethod]
        public void PayrollPreview_NoDiscountOnEmployeeWithDependent()
        {
            DependencyContainer.RegisterType<IBenefitRepository, LocalBenefitRepository>();
            var benefits = DependencyContainer.Resolve<IBenefitRepository>().GetBenefitPackages();

            var logic = new PayrollLogic();
            var employee = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            employee.Dependents.Add(new Dependent()
            {
                FirstName = "Luke",
                LastName = "Skywalker"
            });

            var result = logic.CreatePreview(employee);
            Assert.IsNotNull(result, "Payroll preview result is not null.");
            Assert.IsNotNull(result.Employee, "Employee on payroll preview is not null");
            Assert.IsNotNull(result.Paychecks, "Paychecks on payroll preview is not null");
            Assert.IsTrue(result.Paychecks.Count > 0, "Paychecks on payroll preview has records.");

            decimal totalDeductionsOnEntity = result.PayrollDeductions;
            decimal totalDeductionsOnPaychecks = result.Paychecks.Sum(p => p.PayrollDeductions.Sum(d => d.Cost));

            Assert.AreEqual(totalDeductionsOnEntity, totalDeductionsOnPaychecks);
            Assert.AreEqual(benefits.Sum(b => b.EmployeeCost.NetCost + b.DependentCost.NetCost), totalDeductionsOnPaychecks);
        }

        [TestMethod]
        public void PayrollPreview_HasDiscountOnSoleEmployee()
        {
            var logic = new PayrollLogic();
            var employee = new Employee()
            {
                FirstName = "Anakin",
                LastName = "Skywalker"
            };

            var result = logic.CreatePreview(employee);
            Assert.IsNotNull(result, "Payroll preview result is not null.");
            Assert.IsNotNull(result.Employee, "Employee on payroll preview is not null");
            Assert.IsNotNull(result.Paychecks, "Paychecks on payroll preview is not null");
            Assert.IsTrue(result.Paychecks.Count > 0, "Paychecks on payroll preview has records.");

            Assert.IsTrue(result.EnrolledBenefits.Sum(b => b.EmployeeCost.Discounts) > 0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void PayrollPreview_ExceptionOnNullEmployee()
        {
            var logic = new PayrollLogic();
            var result = logic.CreatePreview(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void PayrollPreview_ExceptionInvalidEmployee()
        {
            var logic = new PayrollLogic();
            var result = logic.CreatePreview(new Employee());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void PayrollPreview_ExceptionOnNullDependent()
        {
            var logic = new PayrollLogic();
            var employee = new Employee();
            employee.Dependents.Add(null);

            var result = logic.CreatePreview(employee);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), AllowDerivedTypes = true)]
        public void PayrollPreview_ExceptionOnInvalidDependent()
        {
            var logic = new PayrollLogic();
            var employee = new Employee();
            employee.Dependents.Add(new Dependent());

            var result = logic.CreatePreview(employee);
        }
    }
}
