﻿using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Components.Configuration;

namespace Payroll.Components.Tests
{
    [TestClass]
    public class BenefitConfigurationTests
    {
        [TestMethod]
        public void BenefitConfiguration_GetConfigFromAppConfig()
        {
            var section = ConfigurationManager.GetSection("benefitConfiguration") as BenefitConfigurationSection;
            Assert.IsNotNull(section);
        }

        [TestMethod]
        public void BenefitConfiguration_CorrectValuesFromAppConfig()
        {
            var section = ConfigurationManager.GetSection("benefitConfiguration") as BenefitConfigurationSection;
            Assert.IsNotNull(section);
            Assert.IsNotNull(section.Benefits);
            Assert.IsTrue(section.Benefits.Count > 0);

            var benefitPackage = section.Benefits.BenefitSettings.FirstOrDefault();
            Assert.IsNotNull(benefitPackage);
            Assert.AreEqual(500m, benefitPackage.AnnualDependentCost);
            Assert.AreEqual(1000m, benefitPackage.AnnualEmployeeCost);
        }
    }
}
