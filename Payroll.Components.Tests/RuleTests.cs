﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.BusinessLogic;
using Payroll.Components.BusinessLogic.PayrollRules;

namespace Payroll.Components.Tests
{
    [TestClass]
    public class RuleTests
    {

        [TestMethod]
        public void RuleTests_NonEmptyRuleResult()
        {
            var rules = DiscountLogic.GetAllDiscountRules();

            Assert.IsTrue(rules.Count > 0);
        }

        [TestMethod]
        public void RuleTests_NameStartsWithARule_HasDiscount()
        {
            var rule = new NameBeginsWithADiscount();

            IPerson chosenOne = new Employee()
            {
                FirstName = "Anakin",
                LastName = "Skywalker"
            };

            Assert.IsTrue(rule.GetDiscountPercentage(chosenOne) > 0.0m);
        }

        [TestMethod]
        public void RuleTests_NameStartsWithARule_NoDiscount()
        {
            var rule = new NameBeginsWithADiscount();

            IPerson darkLordOfTheSith = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            // No discount for you... you killed younglings.
            Assert.IsTrue(rule.GetDiscountPercentage(darkLordOfTheSith) == 0.0m);
        }
    }
}
