﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Components.Configuration;

namespace Payroll.Components.Tests
{
    [TestClass]
    public class PayrollConfigurationTests
    {
        [TestMethod]
        public void PayrollConfiguration_CorrectValuesFromConfig()
        {
            var section = ConfigurationManager.GetSection("payrollConfiguration") as PayrollConfigurationSection;
            Assert.IsNotNull(section, "Testing section exists");
            Assert.AreEqual(26, section.PayPeriodsPerYear, "Correct PayPeriodsPerYear from config. Unless you changed the config...you didn't do that did you?");
            Assert.AreEqual(2000m, section.GrossPayPerPeriod, "Correct GrossPayPerPeriod from config. Unless you changed the config...you didn't do that did you?");
        }
    }
}
