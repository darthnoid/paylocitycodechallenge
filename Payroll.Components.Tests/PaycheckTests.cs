﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Common.Entities.Payroll;

namespace Payroll.Components.Tests
{
    [TestClass]
    public class PaycheckTests
    {
        [TestMethod]
        public void PayrollDeductions_NoDeduction()
        {
            decimal grossPay = 2000m;
            var paycheck = new Paycheck(1, grossPay, new List<PayrollDeduction>());
            Assert.AreEqual(grossPay, paycheck.GrossPay, "Testing gross pay with no deductions.");
            Assert.AreEqual(grossPay, paycheck.NetPay, "Testing net pay with no deductions.");
        }

        [TestMethod]
        public void PayrollDeductions_OneDeduction()
        {
            decimal grossPay = 2000m;
            var paycheck = new Paycheck();
            paycheck.GrossPay = grossPay;
            paycheck.PayrollDeductions.Add(new PayrollDeduction(100m));

            Assert.AreEqual(grossPay - paycheck.PayrollDeductions.First().Cost, paycheck.NetPay, "Testing net pay after one deduction");
        }

        [TestMethod]
        public void PayrollDeductions_MultipleDeductions()
        {
            decimal grossPay = 2000m;
            var paycheck = new Paycheck();
            paycheck.GrossPay = grossPay;

            var cheapDeduction = new PayrollDeduction(50m);
            var expensiveDeduction = new PayrollDeduction(150m);

            paycheck.PayrollDeductions.Add(cheapDeduction);
            paycheck.PayrollDeductions.Add(expensiveDeduction);
            Assert.AreEqual(grossPay - (cheapDeduction.Cost + expensiveDeduction.Cost), paycheck.NetPay, "Testing net pay after multiple deductions");
        }
    }
}
