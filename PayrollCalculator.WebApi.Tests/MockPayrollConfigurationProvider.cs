﻿using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;

namespace PayrollWebApi.Tests
{
    public class FiftyTwoPaychecksAYearMockConfigRepository : IPayrollConfigurationRepository
    {
        public IPayrollConfiguration GetConfiguration()
        {
            return new PayrollConfiguration()
            {
                GrossPayPerPeriod = 1000m,
                PayPeriodsPerYear = 52
            };
        }
    }
}
