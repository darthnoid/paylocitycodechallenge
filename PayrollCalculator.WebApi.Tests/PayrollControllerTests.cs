﻿using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Payroll.Common.Entities;
using Payroll.Common.Entities.Payroll;
using Payroll.Common.Interfaces;
using Payroll.Components.BusinessLogic;
using Payroll.Components.Utility;
using PayrollWebApi.Controllers;

namespace PayrollWebApi.Tests
{
    /// <summary>
    /// This tests the Payroll Controller.
    /// We check the type of the return result to determine 
    /// the API is responding to valid and invalid conditions in a predictable way.
    /// </summary>
    [TestClass]
    public class PayrollControllerTests
    {
        [TestMethod]
        public void PayrollController_GetPayrollConfiguration()
        {
            DependencyContainer.RegisterType<IPayrollConfigurationRepository, FiftyTwoPaychecksAYearMockConfigRepository>();

            var result = new PayrollController().GetEmployerSettings() as OkNegotiatedContentResult<EmployerSettings>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
        }

        [TestMethod]
        public void PayrollController_GetPayrollConfigurationPropertiesFromConfig()
        {
            DependencyContainer.RegisterType<IPayrollConfigurationRepository, LocalPayrollConfigurationRepository>();

            var result = new PayrollController().GetEmployerSettings() as OkNegotiatedContentResult<EmployerSettings>;
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
        }

        [TestMethod]
        public void PayrollController_CreatePayrollPreview_CorrectResponseOnInvalidEmployee()
        {
            var result = new PayrollController().CreatePayrollPreview(new Employee()) as BadRequestErrorMessageResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PayrollController_CreatePayrollPreview_BadRequestOnNullEmployee()
        {
            var result = new PayrollController().CreatePayrollPreview(null) as BadRequestErrorMessageResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PayrollController_CreatePayrollPreview_BadRequestOnBadDependent()
        {
            var employee = new Employee();
            employee.FirstName = "Darth";
            employee.LastName = "Vader";

            employee.Dependents.Add(new Dependent());

            var result = new PayrollController().CreatePayrollPreview(employee) as BadRequestErrorMessageResult;
            Assert.IsNotNull(result, "Testing to ensure bad request type is received on bad request.");
        }

        [TestMethod]
        public void PayrollController_CreatePayrollPreview_OkResponseOnGoodEmployee()
        {
            var employee = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            var result = new PayrollController().CreatePayrollPreview(employee) as OkNegotiatedContentResult<PayrollInformation>;
            Assert.IsNotNull(result, "Testing to ensure Ok result on well formed employee.");
        }

        [TestMethod]
        public void PayrollController_CreatePayrollPreview_OkResponseOnGoodEmployeeWithDependents()
        {
            var employee = new Employee()
            {
                FirstName = "Darth",
                LastName = "Vader"
            };

            employee.Dependents.Add(new Dependent()
            {
                FirstName = "Luke",
                LastName = "Skywalker"
            });

            var result = new PayrollController().CreatePayrollPreview(employee) as OkNegotiatedContentResult<PayrollInformation>;
        }
    }
}
