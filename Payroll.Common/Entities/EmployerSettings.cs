﻿using System.Collections.Generic;
using Payroll.Common.Entities.Benefits;
using Payroll.Common.Interfaces;

namespace Payroll.Common.Entities
{
    public class EmployerSettings
    {
        public EmployerSettings(IPayrollConfiguration payrollConfig, List<BenefitPackage> benefits)
        {
            this.BenefitPackages = benefits;
            this.PayrollConfiguration = payrollConfig;
        }

        public List<BenefitPackage> BenefitPackages { get; private set; }

        public IPayrollConfiguration PayrollConfiguration { get; private set; }
    }
}
