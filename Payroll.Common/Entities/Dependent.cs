﻿using Payroll.Common.Interfaces;

namespace Payroll.Common.Entities
{
    public class Dependent : IPerson
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
