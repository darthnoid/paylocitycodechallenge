﻿using System.Collections.Generic;
using Payroll.Common.Interfaces;

namespace Payroll.Common.Entities.Payroll
{
    public class Employee : IPerson
    {
        public Employee()
        {
            this.Dependents = new List<Dependent>();
        }

        public Employee(List<Dependent> dependents)
        {
            this.Dependents = dependents;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public List<Dependent> Dependents { get; private set; }
    }
}