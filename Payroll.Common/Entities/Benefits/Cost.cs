﻿using System;
using System.Collections.Generic;

namespace Payroll.Common.Entities.Benefits
{
    public class Cost
    {
        public Cost()
        { }

        public Cost(decimal grossCost)
        {
            this.GrossCost = grossCost;
        }

        public Cost(decimal grossCost, decimal discounts)
        {
            this.GrossCost = grossCost;
            this.Discounts = discounts;
        }

        /// <summary>
        /// Combines the list of costs into a new cost object.
        /// </summary>
        /// <param name="costs">The costs to be combined.</param>
        /// <returns></returns>
        public static Cost Combine(IEnumerable<Cost> costs)
        {
            if (costs == null) throw new ArgumentNullException(nameof(costs));
            var newCost = new Cost();

            // could have done sums on this container and passed
            // into Cost constructor but didn't want to unnecessarily
            // iterate over the collection twice.
            foreach (var cost in costs)
            {
                newCost.GrossCost += cost.GrossCost;
                newCost.Discounts += cost.Discounts;
            }

            return newCost;
        }

        public decimal GrossCost { get; set; }

        public decimal Discounts { get; set; }

        public decimal NetCost
        {
            get
            {
                return GrossCost - Discounts;
            }
        }
    }
}
