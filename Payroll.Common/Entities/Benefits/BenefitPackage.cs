﻿namespace Payroll.Common.Entities.Benefits
{
    public class BenefitPackage
    {
        public BenefitPackage()
        { }

        public BenefitPackage(string name, Cost employeeCost, Cost dependentCost)
        {
            this.Name = name;
            this.EmployeeCost = employeeCost;
            this.DependentCost = dependentCost;
        }

        public string Name { get; set; }

        public Cost EmployeeCost { get; set; }

        public Cost DependentCost { get; set; }
    }
}
