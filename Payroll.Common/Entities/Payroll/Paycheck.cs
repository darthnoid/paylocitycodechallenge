﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Payroll.Common.Entities.Payroll
{
    public class Paycheck
    {
        public Paycheck()
        {
            this.PayrollDeductions = new List<PayrollDeduction>();
        }

        public Paycheck(int payPeriod, decimal grossPay, List<PayrollDeduction> payrollDeductions)
        {
            this.PayPeriod = payPeriod;
            this.GrossPay = grossPay;
            this.PayrollDeductions = payrollDeductions ?? new List<PayrollDeduction>();
        }

        public int PayPeriod { get; set; }

        public decimal GrossPay { get; set; }

        public decimal NetPay
        {
            get
            {
                return Decimal.Round(GrossPay - PayrollDeductions.Sum(d => d.Cost), 2);
            }
        }

        public List<PayrollDeduction> PayrollDeductions { get; private set; }
    }
}
