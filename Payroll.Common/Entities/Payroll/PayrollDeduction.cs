﻿namespace Payroll.Common.Entities.Payroll
{
    public class PayrollDeduction
    {
        public PayrollDeduction()
        { }

        public PayrollDeduction(decimal cost)
        {
            this.Cost = cost;
        }

        public decimal Cost { get; set; }
    }
}
