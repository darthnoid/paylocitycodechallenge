﻿using Payroll.Common.Interfaces;

namespace Payroll.Common.Entities.Payroll
{
    public class PayrollConfiguration : IPayrollConfiguration
    {
        public decimal GrossPayPerPeriod { get; set; }

        public int PayPeriodsPerYear { get; set; }
    }
}
