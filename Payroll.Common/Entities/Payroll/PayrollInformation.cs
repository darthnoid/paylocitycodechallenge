﻿using System.Collections.Generic;
using System.Linq;

namespace Payroll.Common.Entities.Payroll
{
    public class PayrollInformation
    {
        public PayrollInformation(Employee employee, List<Paycheck> paychecks, List<Benefits.BenefitPackage> enrolledBenefits)
        {
            this.Employee = employee;
            this.Paychecks = paychecks;
            this.EnrolledBenefits = enrolledBenefits;
        }

        public Employee Employee { get; private set; }

        public List<Benefits.BenefitPackage> EnrolledBenefits { get; private set; }

        public List<Paycheck> Paychecks { get; private set; }

        public decimal GrossPay
        {
            get
            {
                return this.Paychecks.Sum(p => p.GrossPay);
            }
        }

        public decimal Discounts
        {
            get
            {
                return this.EnrolledBenefits.Sum(b => b.EmployeeCost.Discounts + b.DependentCost.Discounts);
            }
        }

        public decimal PayrollDeductions
        {
            get
            {
                return this.Paychecks.Sum(p => p.PayrollDeductions.Sum(d => d.Cost));
            }
        }

        public decimal NetPay
        {
            get
            {
                return this.Paychecks.Sum(p => p.NetPay);
            }
        }
    }
}
