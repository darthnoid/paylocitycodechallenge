﻿namespace Payroll.Common.Interfaces
{
    public interface IPayrollConfiguration
    {
        int PayPeriodsPerYear { get; }

        decimal GrossPayPerPeriod { get; }
    }
}
