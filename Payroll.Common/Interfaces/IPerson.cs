﻿namespace Payroll.Common.Interfaces
{
    public interface IPerson
    {
        string FirstName { get; set; }

        string LastName { get; set; }
    }
}
