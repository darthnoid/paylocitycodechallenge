﻿namespace Payroll.Common.Interfaces
{
    public interface IPayrollConfigurationRepository
    {
        IPayrollConfiguration GetConfiguration();
    }
}
