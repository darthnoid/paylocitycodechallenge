﻿using Payroll.Common.Entities.Benefits;
using System.Collections.Generic;

namespace Payroll.Common.Interfaces
{
    public interface IBenefitRepository
    {
        List<BenefitPackage> GetBenefitPackages();
    }
}
